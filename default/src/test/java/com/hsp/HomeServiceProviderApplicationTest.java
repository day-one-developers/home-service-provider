package com.hsp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hsp.core.enums.orderStatus;
import com.hsp.core.input.UserInput;
import com.hsp.core.model.Order;
import com.hsp.core.model.User;
import com.hsp.core.repository.OrderRepository;
import com.hsp.core.repository.UserRepository;
import com.hsp.assigner.service.TechnicianAssignerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
//@ContextConfiguration
@SpringBootTest(classes = HomeServiceProviderApplication.class)
@AutoConfigureMockMvc
class HomeServiceProviderApplicationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private TechnicianAssignerService techAssign;

    @Test
    void registrationWorksThroughAllLayers() throws Exception {

        UserInput user = new UserInput();

        String customerEmail = System.currentTimeMillis() + "govindssgr@gmail.com";
        String technicianEmail = System.currentTimeMillis() + "sampathssgr@gmail.com";

        user.setName("Govindaraj");
        user.setCity("Nagapattinam");
        user.setEmail(customerEmail);
        user.setCountry("India");
        user.setPhone(9445147723L);
        user.setPinCode(611001L);
        user.setPwd("govind");
        user.setState("Tamilnadu");
        user.setStreet("velipalayam");
        user.setUserType("CUSTOMER");

        mockMvc.perform( MockMvcRequestBuilders
                        .post("/sign_up")
                        .content(asJsonString(user))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        User users = userRepository.findByEmail(customerEmail);
        assertThat(users.getName()).isEqualTo("Govindaraj");

        UserInput use = new UserInput();

        use.setName("Sampath");
        use.setCity("nagai");
        use.setEmail(technicianEmail);
        use.setCountry("India");
        use.setPhone(9443259523L);
        use.setPinCode(611001L);
        use.setPwd("sampath");
        use.setState("Tamilnadu");
        use.setServiceId(1L);
        use.setExperience(4L);
        use.setStreet("Velipalayam");
        use.setUserType("TECHNICIAN");

        mockMvc.perform( MockMvcRequestBuilders
                .post("/sign_up")
                .content(asJsonString(use))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        User tech = userRepository.findByEmail(technicianEmail);
        assertThat(tech.getName()).isEqualTo("Sampath");

        mockMvc.perform( MockMvcRequestBuilders
                .post("/customer/book_service")
                        .param("customerId", users.getId().toString())
                        .param("serviceId", "1")
                        .param("timestamp", "67583762")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON));

        Order order = orderRepository.findByCustomerId(users.getId());

        assertThat(order.getStatus()).isEqualTo(orderStatus.INITIAED.name());

//        boolean success = techAssign.assignTechnician(order.getId());
//
//        order = orderRepository.findByCustomerId(users.getId());
//        if (success) {
//            assertThat(order.getStatus()).isEqualTo(orderStatus.ASSIGNED.name());
//        } else {
//            assertThat(order.getStatus()).isEqualTo(orderStatus.INITIAED.name());
//        }


    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }



}
