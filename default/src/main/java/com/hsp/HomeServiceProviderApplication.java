package com.hsp;

//import com.hsp.core.service.JWTAuthorization;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
@ComponentScan(basePackages = { "com.hsp.*" })
public class HomeServiceProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeServiceProviderApplication.class, args);
	}

//	@EnableWebSecurity
//	@Configuration
//	class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//
//		@Override
//		protected void configure(HttpSecurity http) throws Exception {
//			http.csrf().disable()
//					.addFilterAfter(new JWTAuthorization(), UsernamePasswordAuthenticationFilter.class)
//					.authorizeRequests()
//					.antMatchers(HttpMethod.GET, "/verify").permitAll()
//					.antMatchers(HttpMethod.POST, "/login").permitAll()
//					.antMatchers(HttpMethod.POST, "/sign_up").permitAll()
//					.antMatchers(HttpMethod.GET, "/technician/order_history").permitAll()
//					.antMatchers(HttpMethod.GET, "/technician/assigned_serives").permitAll()
//					.anyRequest().authenticated();
//		}
//	}
}