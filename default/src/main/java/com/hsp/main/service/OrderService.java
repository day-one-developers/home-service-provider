package com.hsp.main.service;

import com.hsp.core.enums.orderStatus;
import com.hsp.core.enums.UserType;
import com.hsp.core.input.ComplaintInput;
import com.hsp.core.model.Complaint;
import com.hsp.core.model.Order;
import com.hsp.core.model.User;
import com.hsp.core.repository.ComplaintRepository;
import com.hsp.core.repository.OrderRepository;
import com.hsp.core.repository.ServiceRepository;
import com.hsp.core.repository.UserRepository;
import com.hsp.main.view.CustomerOrderView;
import com.hsp.main.view.OrderView;
import com.hsp.main.view.TechnicianOrderView;
import org.springframework.beans.factory.annotation.Autowired;
import com.hsp.core.model.HomeService;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class OrderService {

    private UserRepository userRepository;
    private OrderRepository orderRepository;
    private ComplaintRepository complaintRepository;
    private ServiceRepository serviceRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, ComplaintRepository complaintRepository, UserRepository userRepository, ServiceRepository serviceRepository) {
        this.orderRepository = orderRepository;
        this.complaintRepository = complaintRepository;
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
    }

    public OrderView getOrderDetails(Long orderId) {
        Order order = orderRepository.findById(orderId).orElse(null);
        if (order == null) return null;
        HomeService service = serviceRepository.findById(order.getServiceId()).orElse(null);
        User user = userRepository.findById(order.getCustomerId()).orElse(null);
        OrderView formatedData = new TechnicianOrderView();
        formatedData.setOrderId(order.getId());
        formatedData.setOrderDate(order.getOrderDate());
        formatedData.setStatus(order.getStatus());
        if (service != null)
            formatedData.setServiceName(service);
        if (user != null) {
            formatedData.setName(user.getName());
            formatedData.setNumber(user.getPhone());
        }
        return formatedData;
    }

    public List<OrderView> getOrderHistory(Long userId, UserType type, int pageNum, int rowCount) {
        final List<Order> orders;
        if (UserType.TECHNICIAN.equals(type)) {
            orders = orderRepository.findAllTechnician(userId, (pageNum - 1) * rowCount, pageNum * rowCount);
            return formatData(orders, UserType.TECHNICIAN);
        } else {
            orders = orderRepository.findAllCustomer(userId, (pageNum - 1) * rowCount, pageNum * rowCount);
            return formatData(orders, UserType.CUSTOMER);
        }
    }

    public List<OrderView> findByTechnicianId(Long technicianId) {
        List<Order> order = orderRepository.findByTechnicianIdAndStatus(technicianId, orderStatus.ASSIGNED.name());

        return formatData(order, UserType.TECHNICIAN);
    }

    public void report(ComplaintInput complaintInput, String complaintType) {
        Complaint complaint = new Complaint();
        complaint.setComplaintType(complaintType);
        complaint.setMessage(complaintInput.getMessage());
        complaint.setOrderId(complaintInput.getOrderId());
        complaint.setUserId(complaintInput.getUserId());

        // System.currentTimeMillis() function returns different time in different locality.
        complaint.setTimestamp(System.currentTimeMillis());
        complaintRepository.save(complaint);
    }

    public boolean updateStatus(Long orderId, String status) {
        Order order = orderRepository.findById(orderId).orElseThrow();
        order.setStatus(status);
        try {
            orderRepository.save(order);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void createNewOrder(Long cid, Long sid, Long date) {
        Order order = new Order();
        order.setServiceId(sid);
        order.setCustomerId(cid);
        order.setStatus(orderStatus.INITIAED.name());
        order.setOrderDate(date);
        orderRepository.save(order);
        // TODO(ruppa) Technician assigner service implementation
    }

    private List<OrderView> formatData(List<Order> order, UserType type) {
        Map<Long, List<OrderView>> orderMap = new HashMap<>();
        Map<Long, List<OrderView>> serviceOrder = new HashMap<>();
        List<Long> ids = new ArrayList<>();
        List<Long> serviceIds = new ArrayList<>();
        List<OrderView> list = new LinkedList<>();
        for (Order o : order) {
            OrderView orderView;
            if(UserType.TECHNICIAN.equals(type)) {
                orderView = new TechnicianOrderView();
                if (!orderMap.containsKey(o.getCustomerId())) {
                    orderMap.put(o.getCustomerId(), new LinkedList<OrderView>());
                }
                orderMap.get(o.getCustomerId()).add(orderView);
                ids.add(o.getCustomerId());
            } else {
                orderView = new CustomerOrderView();
                if (!orderMap.containsKey(o.getTechnicianId())) {
                    orderMap.put(o.getTechnicianId(), new LinkedList<OrderView>());
                }
                orderMap.get(o.getTechnicianId()).add(orderView);
                ids.add(o.getTechnicianId());
            }
            orderView.setOrderId(o.getId());
            orderView.setStatus(o.getStatus());
            orderView.setOrderDate(o.getOrderDate());
            if (!serviceOrder.containsKey(o.getServiceId())) {
                serviceOrder.put(o.getServiceId(), new LinkedList<OrderView>());
            }
            serviceOrder.get(o.getServiceId()).add(orderView);
            serviceIds.add(o.getServiceId());
            list.add(orderView);
        }
        List<User> users = userRepository.findByIdIn(ids);
        List<HomeService> services = serviceRepository.findByIdIn(serviceIds);

        for (User user : users) {
            List<OrderView> o = orderMap.get(user.getId());
            if (o != null)
                for (OrderView view : o) {
                    view.setName(user.getName());
                    view.setNumber(user.getPhone());
                }
        }
        for (HomeService service : services) {
            List<OrderView> o = serviceOrder.get(service.getId());
            for (OrderView view : o)
                view.setServiceName(service);
        }
        return list;
    }
}
