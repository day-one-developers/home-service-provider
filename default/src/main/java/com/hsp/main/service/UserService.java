package com.hsp.main.service;

import com.hsp.core.enums.UserType;
import com.hsp.core.input.UserInput;
import com.hsp.core.model.Location;
import com.hsp.core.model.Technician;
import com.hsp.core.model.User;
import com.hsp.core.repository.LocationRepository;
import com.hsp.core.repository.TechnicianRepository;
import com.hsp.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class UserService {

//    @Autowired
//    PasswordEncoder passwordEncoder;

//    @Autowired
//    JavaMailSender mailSender;
    private UserRepository userRepository;
    private LocationRepository locationRepository;
    private TechnicianRepository technicianRepository;

    @Autowired
    UserService(UserRepository userRepository, LocationRepository locationRepository, TechnicianRepository technicianRepository) {
        this.userRepository = userRepository;
        this.locationRepository = locationRepository;
        this.technicianRepository = technicianRepository;
    }

    public User createOrUpdateUser(UserInput userInput) {
        // TODO(ruppa): add email and phone number verification code before saving it.
        User user = (userInput.getId() == null) ? newUser() : userRepository.findById(userInput.getId())
                .orElseThrow();
        Location location = (userInput.getLocationId() == null) ? newLocation(userInput) : locationRepository.findById(userInput.getLocationId()).orElse(null);
        user.setEmail(userInput.getEmail());
        user.setName(userInput.getName());
        user.setUserType(userInput.getUserType());
        user.setPhone(userInput.getPhone());
        user.setPwd(userInput.getPwd());
        user.setLocationId(location.getId());

        User u = userRepository.save(user);
        if (userInput.getUserType() != null && userInput.getUserType().equals(UserType.TECHNICIAN.name())) {
            Technician technician = new Technician();
            technician.setServiceId(userInput.getServiceId());
            technician.setUserId(u.getId());
            technician.setExperience(userInput.getExperience());
            Technician tech = technicianRepository.save(technician);
        }
        return user;
    }

    private Location newLocation(UserInput userInput) {
        Location location = new Location();
        location.setStreet(userInput.getStreet());
        location.setCity(userInput.getCity());
        location.setPin_code(userInput.getPinCode());
        location.setCountry(userInput.getCountry());
        location.setState(userInput.getState());
        locationRepository.save(location);
        return location;
    }

    private User newUser() {
        User user = new User();
        // TODO(ruppa) Introduce a status for this purpose
        user.setEnabled(false);
        Random random = new Random();
        user.setVerification(random.nextInt(90) + 10);
        return user;
    }

    public Boolean checkUserCredentials(String email, String pwd) {
        User user = userRepository.findByEmail(email);
        return user.getPwd().equals(pwd);
    }
}
