package com.hsp.main.controller;

import com.hsp.main.exception.ResourceNotFoundException;
import com.hsp.core.enums.UserType;
import com.hsp.main.service.OrderService;
import com.hsp.main.view.OrderView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/technician")
public class TechnicianController {

    private OrderService orderService;

    @Autowired
    public TechnicianController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/assigned_services")
    public List<OrderView> listAssignedServices(@RequestParam Long id) {
        return orderService.findByTechnicianId(id);
    }

    @PatchMapping("/update_status")
    public void updateStatus(@RequestParam Long orderId, @RequestParam String status) {
        if (!orderService.updateStatus(orderId, status)) {
            throw new ResourceNotFoundException();
        }
    }

    @GetMapping("/order_history")
    public List<OrderView> orderHistory(@RequestParam Long userId, @RequestParam int pageNum, @RequestParam int rowCount) {
        return orderService.getOrderHistory(userId, UserType.TECHNICIAN, pageNum, rowCount);
    }

}
