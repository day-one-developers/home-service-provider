package com.hsp.main.controller;

import com.hsp.core.input.ComplaintInput;
import com.hsp.main.service.OrderService;
import com.hsp.main.view.OrderView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/order_details")
    public OrderView orderDetails(@RequestParam Long orderId) {
        return orderService.getOrderDetails(orderId);
    }

    @PutMapping("/report_service")
    public void reportService(@RequestBody ComplaintInput complaintInput, @RequestParam String type) {
        orderService.report(complaintInput, type);
    }


}
