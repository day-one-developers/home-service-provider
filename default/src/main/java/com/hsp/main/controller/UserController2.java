//package com.hsp.controller;
//
//import ResourceNotFoundException;
//import UserInput;
//import com.hsp.core.model.User;
//import UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import javax.servlet.http.HttpServletRequest;
//
//@RestController
//public class UserController2 {
//
//    private UserService userService;
//
//    @Autowired
//    UserController2(UserService userService) {
//        this.userService = userService;
//    }
//
//    @PostMapping("/sign_up")
//    public User synUp (@RequestBody UserInput userInput) {
//
//        User user = userService.createNewUserEntry(userInput);
//
//        return user;
//    }
//    private String getSiteURL(HttpServletRequest request) {
//        String siteURL = request.getRequestURL().toString();
//        return siteURL.replace(request.getServletPath(), "");
//    }
//
//    @PatchMapping("/profile_update")
//    public void profileUpdate (@RequestBody UserInput userInput) {
//        userService.editUserDetails(userInput);
//    }
//
//    @PostMapping("/login")
//    public String login(@RequestParam String email, @RequestParam String pwd) {
//
//        if(!userService.checkUserCredentials(email, pwd)) {
//            throw new ResourceNotFoundException();
//        }
//
//        return "success";
//
//    }
//
////    private String getJWTToken(String username) {
////        String secretKey = "mySecretKey";
////        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
////                .commaSeparatedStringToAuthorityList("ROLE_USER");
////
////        String token = Jwts
////                .builder()
////                .setId("softtekJWT")
////                .setSubject(username)
////                .claim("authorities",
////                        grantedAuthorities.stream()
////                                .map(GrantedAuthority::getAuthority)
////                                .collect(Collectors.toList()))
////                .setIssuedAt(new Date(System.currentTimeMillis()))
////                .setExpiration(new Date(System.currentTimeMillis() + 600000))
////                .signWith(SignatureAlgorithm.HS512,
////                        secretKey.getBytes()).compact();
////
////        return "Bearer " + token;
////    }
////
////    @GetMapping("/verify")
////    public String verifyUser(@RequestParam("code") String code) {
////        if (userService.verify(code)) {
////            return "verify_success";
////        } else {
////            return "verify_fail";
////        }
////    }
//}
