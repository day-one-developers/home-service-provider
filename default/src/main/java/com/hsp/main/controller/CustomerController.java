package com.hsp.main.controller;

import com.hsp.main.exception.ResourceNotFoundException;
import com.hsp.main.service.OrderService;
import com.hsp.main.view.OrderView;
import com.hsp.core.enums.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@SpringBootApplication(scanBasePackages = "core")
@RestController
@RequestMapping("/customer")
public class CustomerController {

    private OrderService orderService;

    @Autowired
    public CustomerController (OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/book_service")
    // TODO(ruppa) Get a service booking request instead of params
    public void bookService (@RequestParam Long customerId, @RequestParam Long serviceId, @RequestParam Long timestamp) {
        if (timestamp == null || serviceId == null) {
            throw new ResourceNotFoundException();
        }
        orderService.createNewOrder(customerId, serviceId, timestamp);
    }

    @GetMapping("/order_history")
    public List<OrderView> orderHistory(@RequestParam Long userId, @RequestParam int pageNum, @RequestParam int rowCount) {
        return orderService.getOrderHistory(userId, UserType.CUSTOMER, pageNum, rowCount);
    }

}
