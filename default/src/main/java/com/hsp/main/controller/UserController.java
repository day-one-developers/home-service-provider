package com.hsp.main.controller;

import com.hsp.main.exception.ResourceNotFoundException;
import com.hsp.core.input.UserInput;
import com.hsp.core.model.User;
import com.hsp.main.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserController {

    private UserService userService;

    @Autowired
    UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign_up")
    public User signUp(@RequestBody UserInput userInput) {
        System.out.println(userInput.getEmail());
        return userService.createOrUpdateUser(userInput);
    }

    @PatchMapping("/profile_update")
    public void profileUpdate(@RequestBody UserInput userInput, HttpServletRequest request) {
        userService.createOrUpdateUser(userInput);
    }

    // TODO(ruppa) Change the flow to Spring security. This is a temporary thing
    @PostMapping("/login")
    public String login(@RequestParam String email, @RequestParam String pwd) {
        if (!userService.checkUserCredentials(email, pwd)) {
            throw new ResourceNotFoundException();
        }

        return "success";
    }
}
