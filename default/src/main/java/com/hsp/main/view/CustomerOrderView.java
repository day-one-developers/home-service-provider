package com.hsp.main.view;

public class CustomerOrderView extends OrderView{
    private String customerName;
    private Long customerNumber;

    public String getName() {return customerName;}

    public void setName(String customerName) {this.customerName = customerName;}

    public Long getNumber() {return customerNumber;}

    public void setNumber(Long customerNumber) {this.customerNumber = customerNumber;}
}
