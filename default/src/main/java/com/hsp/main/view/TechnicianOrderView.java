package com.hsp.main.view;

public class TechnicianOrderView extends OrderView {

    private String technicianName;
    private Long technicianNumber;

    public String getName() {return technicianName;}

    public void setName(String technicianName) {this.technicianName = technicianName;}

    public Long getNumber() {return technicianNumber;}

    public void setNumber(Long technicianNumber) {this.technicianNumber = technicianNumber;}

}
