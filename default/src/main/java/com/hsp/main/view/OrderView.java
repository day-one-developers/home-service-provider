package com.hsp.main.view;

import com.hsp.core.model.HomeService;

public abstract class OrderView {
    Long orderId;
    Long orderDate;
    String status;
    HomeService serviceObj;

    public Long getOrderId() {return orderId;}

    public void setOrderId(Long orderId) {this.orderId = orderId;}

    public HomeService getServiceName() {return serviceObj;}

    public void setServiceName(HomeService serviceObj) {this.serviceObj = serviceObj;}

    public Long getOrderDate() {return orderDate;}

    public void setOrderDate(Long orderDate) {this.orderDate = orderDate;}

    public String getStatus() {return status;}

    public void setStatus(String status) {this.status = status;}

    public abstract String getName();

    public abstract Long getNumber();

    public abstract void setNumber(Long phone);

    public abstract void setName(String name);
}
