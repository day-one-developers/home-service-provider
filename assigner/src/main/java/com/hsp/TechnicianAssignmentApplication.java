package com.hsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.hsp.*" })
//@ComponentScan(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = OrderController.class))
public class TechnicianAssignmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TechnicianAssignmentApplication.class, args);
    }

}