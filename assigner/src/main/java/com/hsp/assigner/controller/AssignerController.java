package com.hsp.assigner.controller;

import com.hsp.assigner.service.TechnicianAssignerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AssignerController {
    private TechnicianAssignerService technicianAssignerService;

    @Autowired
    AssignerController(TechnicianAssignerService technicianAssignerService) {
        this.technicianAssignerService = technicianAssignerService;
    }

    @PostMapping("/assign_technician")
    public boolean assign(@RequestParam Long orderId){
        return technicianAssignerService.assignTechnician(orderId);
    }

}
