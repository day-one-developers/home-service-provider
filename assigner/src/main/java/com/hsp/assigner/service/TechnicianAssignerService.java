package com.hsp.assigner.service;

import com.hsp.core.enums.orderStatus;
import com.hsp.core.model.Order;
import com.hsp.core.model.Technician;
import com.hsp.core.repository.OrderRepository;
import com.hsp.core.repository.TechnicianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TechnicianAssignerService {
    TechnicianRepository technicianRepository;
    OrderRepository orderRepository;

    @Autowired
    TechnicianAssignerService (TechnicianRepository technicianRepository, OrderRepository orderRepository) {
        this.technicianRepository = technicianRepository;
        this.orderRepository = orderRepository;
    }

    public boolean assignTechnician(Long orderId) {
        Order order = orderRepository.findById(orderId).orElse(null);
        if (order == null) {
            System.out.println("Order not found for orderId: " + orderId);
            return false;
        }
        if (order.getTechnicianId() != null && order.getStatus() != orderStatus.REJECTED.name()) {
            System.out.println("Techician already assigned for orderId: " + orderId);
            return false;
        }
        String city = orderRepository.findCity(orderId);
        Technician user = technicianRepository.findByServiceIdLimitTo(order.getServiceId(), 1, city, order.getOrderDate());

        if (user != null) {
            order.setTechnicianId(user.getUserId());
            order.setStatus(orderStatus.ASSIGNED.name());
            orderRepository.save(order);
            return true;
        }
        return false;
    }
}
