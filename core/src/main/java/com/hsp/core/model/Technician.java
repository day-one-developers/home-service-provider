package com.hsp.core.model;

import javax.persistence.*;

@Entity
public class Technician {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "service_id")
    private Long serviceId;
    private Long experience;

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public Long getServiceId() {return serviceId;}

    public void setServiceId(Long serviceId) {this.serviceId = serviceId;}

    public Long getExperience() {return experience;}

    public void setExperience(Long experience) {this.experience = experience;}

    public Long getUserId() {return userId;}

    public void setUserId(Long userId) {this.userId = userId;}
}
