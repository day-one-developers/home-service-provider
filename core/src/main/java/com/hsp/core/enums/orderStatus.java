package com.hsp.core.enums;

public enum orderStatus {
    INITIAED,
    ASSIGNED,
    IN_PROGRESS,
    COMPLETED,
    REJECTED
}
