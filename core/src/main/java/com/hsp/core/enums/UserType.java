package com.hsp.core.enums;

public enum UserType {
    CUSTOMER,
    TECHNICIAN
}
