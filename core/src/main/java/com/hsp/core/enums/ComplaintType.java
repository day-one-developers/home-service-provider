package com.hsp.core.enums;

public enum ComplaintType {
    TECHNICIAN,
    CUSTOMER
}
