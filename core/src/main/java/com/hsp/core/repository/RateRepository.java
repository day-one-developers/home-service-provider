package com.hsp.core.repository;

import com.hsp.core.model.Rate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RateRepository extends CrudRepository <Rate, String> {
}
