package com.hsp.core.repository;

import com.hsp.core.model.Location;
import com.hsp.core.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User save(User user);

    User findByEmail(String email);

    Location findByLocationId(Long locationId);

    @Query(value="select * from user where id in (:ids)", nativeQuery = true)
    List<User> findByIdIn(@Param("ids") List<Long> customerIds);

    User findByVerification(String verificationCode);

//    User findByServiceIdLimitTo(Long serviceId, int i);
}
