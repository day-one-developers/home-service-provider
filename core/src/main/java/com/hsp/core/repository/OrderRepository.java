package com.hsp.core.repository;

import com.hsp.core.model.Order;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

    Optional<Order> findById(Long orderId);

    List<Order> findByTechnicianIdAndStatus(Long technicianId, String assigned);

    @Query(value="select * from orders o where o.technician_id = :id limit :from, :to", nativeQuery = true)
    List<Order> findAllTechnician(@Param("id") Long id, @Param("from") int from, @Param("to") int to);

    @Query(value="select * from orders o where o.customer_id = :id limit :from, :to", nativeQuery = true)
    List<Order> findAllCustomer(@Param("id") Long id, @Param("from") int from, @Param("to") int to);

    Order findByCustomerId(Long id);

    @Query(value="select l.city from orders as o join user as u on o.customer_id = u.id join location as l on l.id = u.location_id where o.id = :orderId", nativeQuery = true)
    String findCity(Long orderId);
}
