package com.hsp.core.repository;

import com.hsp.core.model.Complaint;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ComplaintRepository extends CrudRepository<Complaint, Long> {
    Complaint save(Complaint complaint);
}
