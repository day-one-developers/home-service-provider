package com.hsp.core.repository;

import com.hsp.core.model.Log;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends CrudRepository <Log, String>{
}
