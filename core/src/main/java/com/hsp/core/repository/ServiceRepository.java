package com.hsp.core.repository;

import com.hsp.core.model.HomeService;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

// https://www.baeldung.com/spring-data-jdbc-intro

@Repository
public interface ServiceRepository extends CrudRepository<HomeService, Long> {

    @Query(value="select * from service where id in (:ids)", nativeQuery = true)
    List<HomeService> findByIdIn (@Param("ids") List<Long> services);

    Optional<HomeService> findById (Long id);
}
