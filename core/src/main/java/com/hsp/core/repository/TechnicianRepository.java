package com.hsp.core.repository;

import com.hsp.core.model.Technician;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TechnicianRepository extends CrudRepository<Technician, Long> {

    @Query(value="select * from technician as t join user as u on t.user_id = u.id join location as l on u.location_id = l.id where t.service_id = :serviceId and l.city = :city and t.user_id not in (select technician_id from orders where technician_id is not null and (status != 'COMPLETED' and status != 'REJECTED') and order_date = :orderDate) limit :i", nativeQuery = true)
    Technician findByServiceIdLimitTo(Long serviceId, int i, String  city, Long orderDate);
}
