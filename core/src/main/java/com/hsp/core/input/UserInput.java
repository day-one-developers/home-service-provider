package com.hsp.core.input;

public class UserInput {

    private Long id;
    private String name;
    private String userType;
    private String email;
    private Long phone;
    private String pwd;
    private Long locationId;
    private String street;
    private String city;
    private String state;
    private String country;
    private Long pinCode;
    private Long serviceId;
    private Long experience;

    public String getPwd() {return pwd;}

    public void setPwd(String pwd) {this.pwd = pwd;}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public Long getId() {return id;}

    public void setId(Long id) {this.id = id;}

    public String getStreet() {return street;}

    public void setStreet(String street) {this.street = street;}

    public String getCity() {return city;}

    public void setCity(String city) {this.city = city;}

    public String getState() {return state;}

    public void setState(String state) {this.state = state;}

    public String getCountry() {return country;}

    public void setCountry(String country) {this.country = country;}

    public Long getPinCode() {return pinCode;}

    public void setPinCode(Long pinCode) {this.pinCode = pinCode;}

    public Long getServiceId() {return serviceId;}

    public void setServiceId(Long serviceId) {this.serviceId = serviceId;}

    public Long getExperience() {return experience;}

    public void setExperience(Long experience) {this.experience = experience;}

    public Long getLocationId() {return locationId;}

    public void setLocationId(Long locationId) {this.locationId = locationId;}
}
